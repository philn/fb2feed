Fb2Feed
==========

Fb2Feed transforms public Facebook pages and groups to Atom (or RSS) feeds.
Facebook events shared in pages or groups are parsed and stored into iCal feeds.

Installation
------------

0. (Optional) For FB groups and events support, install [Fluyt](https://gitlab.com/philn/fluyt)
1. Install dependencies:

        $ sudo apt install libsystemd-dev
        $ pip3 install --user -r requirements.txt
        
2. Configure:

        $ cp fb2feed.example.ini ~/.config/
        $ vi ~/.config/fb2feed.ini
        
3. Run:

        $ python3 -m fb2feed
        
4. Setup a crontab or systemd timer unit:

        $ # Edit systemd/fb2feed.service at your convenience.
        $ mkdir -p ~/.config/systemd/user
        $ cp systemd/* ~/.config/systemd/user/
        $ systemctl --user enable fb2feed
        $ systemctl --user start fb2feed

5. Point your feed reader to the url of the configured feeds
6. Profit

Contact
-------

I usually hang out on Freenode IRC, in \#gstreamer using the philn
nickname. Feel free to also reach out by mail (check git logs to find my
address).
