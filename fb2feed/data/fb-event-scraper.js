var name = document.querySelector('div[id="title_subtitle"]').querySelector("h1").textContent;
var tables = document.querySelectorAll("table");
var event_location = "";
if (tables.length > 1) {
    var link = tables[1].querySelector("a");
    if (link)
        event_location = link.textContent;
}

var start_date = "";
var end_date = "";
for (var node of document.querySelectorAll('[id="event_time_info"]')) {
    for (var div of node.querySelectorAll("div")) {
        var content = div.getAttribute("content");
        if (content) {
            const tokens = content.split(" ");
            start_date = tokens[0];
            end_date = tokens[tokens.length - 1];
            break;
        }
    }
    if (start_date && end_date)
        break;
}

var description = document.querySelector('[id="reaction_units"]').children[0].querySelectorAll("div")[5].textContent;

var r = {"start_date": start_date, "end_date": end_date, "location": event_location,
         "description": description, "name": name};
r
