function url_without_parameters(url) {
    return url.split('?')[0];
}

var posts = {};
var events = [];

for (var wrapper of document.querySelectorAll('div.userContentWrapper')) {
    var abbrs = wrapper.querySelectorAll('abbr');
    if (!abbrs.length)
        continue;
    var abbr = abbrs[0];
    var href = abbr.parentElement.getAttribute("href");
    if (!href)
        continue;

    var post_id = abbr.getAttribute("data-utime");
    var txt = "";
    for (var div of wrapper.querySelectorAll('div.userContent[data-testid="post_message"]')) {
        var p = div.querySelector("p");
        if (!p)
            continue;
        txt += p.textContent;
    }

    for (var link of wrapper.querySelectorAll('a[data-hovercard-prefer-more-content-show="1"]')) {
        var link_href = link.getAttribute("href");
        if (link_href.indexOf("/events") != -1) {
            var absolute_href = new URL(link_href, document.location).href;
            events.push(url_without_parameters(absolute_href));
        }
    }

    var picture_links = [];
    var video_links = [];
    for (var link of wrapper.querySelectorAll('a[rel="theater"]')) {
        var img_url = link.getAttribute("data-ploi");
        if (img_url) {
            picture_links.push(img_url);
        } else {
            var video_url = link.getAttribute("href");
            if (video_url && video_url.indexOf("video") != -1) {
                var absolute_href = new URL(video_url, document.location).href;
                video_links.push(absolute_href);
            }
        }
    }

    if (!txt && !picture_links.length && !video_links.length)
        continue;

    var author_name = "";
    var clearfix = wrapper.querySelector("div.clearfix");
    if (clearfix) {
       var span = clearfix.querySelector("span");
       if (span)
          author_name = span.getAttribute("title");
    }

    var post = {};
    post["text"] = txt;
    post["href"] = url_without_parameters(new URL(href, document.location).href);
    post["picture_links"] = picture_links;
    post["video_links"] = video_links;
    post["author"] = author_name;
    posts[post_id] = post;
}

var title = document.title;
var r = {"title": title, "posts": posts, "events": events};
r
