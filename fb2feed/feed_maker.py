# Copyright (c) 2019 - Philippe Normand

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

from feedgen.feed import FeedGenerator
import asyncio
import configparser
import datetime
import dateutil.parser
import ics
import os, sys
import pytz
import logging

from . import __version__, utils, network

class FeedMaker:

    def __init__(self, config, options):
        self._config = config
        self._options = options

    def collect_tasks(self):
        tasks = [self.fb_to_feed('page', fb_id, {}) for fb_id in self._config["general"]["pages"]]
        tasks.extend([self.fb_to_feed('group', group, cfg)
                      for group, cfg in self._config["groups"].items()
                      if not cfg.get('disabled', False)])
        return tasks

    async def fb_to_feed(self, fb_type, fb_id, config):
        tasks = []
        events = []

        default_section = self._config['general']
        root_dir = default_section['root_directory']
        media_dir = default_section['media_directory']
        media_url_slug = default_section['media_url_slug']
        fmt = default_section.get('format', 'atom')
        generate_ical_feeds = default_section.get('generate_ical_feeds', False)

        logging.debug("Processing fb_id %s" % fb_id)

        async with network.Client() as client:
            if fb_type == 'group':
                feed_name = config.get("label", fb_id)
                feed_filename = feed_name
                fb_url = 'https://www.facebook.com/groups/%s/' % config['id']
            elif fb_type == 'page':
                feed_filename = fb_id
                fb_url = 'https://www.facebook.com/%s/posts' % fb_id
            else:
                logging.error('Unsupported fb_id type, currently supported: page and group')
                return


            page = await client.fetch_with_fluyt(fb_url, "fb-scraper.js", "json", self._options.persistent_fluyt)
            if not page:
                logging.warning("Not data scraped for %s" % fb_url)
                return

            if fb_type == 'page':
                feed_name = page["title"].split("-")[0].strip()

            logging.info('Generating %s feed' % feed_name)

            fg = FeedGenerator()
            fg.generator("Fb2Feed", version=__version__)
            fg.id(fb_url)
            fg.title(feed_name)

            # profile_section = page.find_all('a', attrs={"aria-label": "Profile picture"})
            # print(profile_section)
            # fg.logo()

            for event_url in page["events"]:
                event = await client.fetch_with_fluyt(event_url, "fb-event-scraper.js", "json", self._options.persistent_fluyt)
                if not event:
                    continue
                events.append(self.parse_fb_event(event_url, event))

            if not page.get("posts"):
                logging.warning('No post for %s' % fb_id)
                return

            newest_post_ts = 0
            for post_id, post in page["posts"].items():
                post_url = post["href"]
                post_id_ts = int(post_id)
                timestamp = datetime.datetime.utcfromtimestamp(post_id_ts)
                timestamp = timestamp.replace(tzinfo=pytz.utc)

                newest_post_ts = max(newest_post_ts, post_id_ts)

                txt = post["text"]
                extra_markup = []
                for img_url in post["picture_links"]:
                    tasks.append(asyncio.ensure_future(client.fetch(img_url, media_dir)))
                    extra_markup.append('<img src="%s%s"/>' % (media_url_slug, utils.url_filename(img_url)))
                for video_url in post["video_links"]:
                    tasks.append(asyncio.ensure_future(client.video_fetch(video_url, media_dir)))
                    extra_markup.append('<video controls src="%s%s.mp4"/>' % (media_url_slug, utils.video_or_event_id(video_url)))

                entry_content = txt + ''.join(extra_markup)
                if not entry_content:
                    continue

                author_name = post.get("author", fb_id)
                entry_title = ' '.join(txt.split(' ')[:15]) or "no title"

                fe = fg.add_entry()
                fe.author(name=author_name, email="%s.facebook.no-reply@fb2feed.org" % fb_id)
                fe.id(post_id)
                fe.link(href=post_url, rel="alternate")
                fe.published(timestamp)
                fe.updated(timestamp)
                fe.title(entry_title)
                fe.content(entry_content, type="html")

            timestamp = datetime.datetime.utcfromtimestamp(newest_post_ts)
            timestamp = timestamp.replace(tzinfo=pytz.utc)
            fg.updated(timestamp)

            logging.debug("%d media download tasks for feed %s" % (len(tasks), feed_name))
            await asyncio.gather(*tasks)

            if fmt == 'atom':
                fg.atom_file(os.path.join(root_dir, '%s.atom.xml' % feed_filename))
            elif fmt == 'rss':
                fg.description(feed_name)
                fg.link(href=fb_url, rel='self')
                fg.rss_file(os.path.join(root_dir, '%s.rss.xml' % feed_filename))

            if events:
                calendar = ics.Calendar(creator="Fb2Feed %s" % __version__)
                for event in events:
                    calendar.events.add(event)

                ical_path = os.path.join(root_dir, '%s.ics' % feed_filename)
                with open(ical_path, 'w') as ical:
                    ical.writelines(calendar)

    def parse_fb_event(self, event_url, event):
        start_date = dateutil.parser.parse(event["start_date"])
        end_date = dateutil.parser.parse(event["end_date"])
        event_id = utils.video_or_event_id(event_url)
        event = ics.event.Event(name=event["name"], begin=start_date, end=end_date,
                                description=event["description"],
                                url=event_url, location=event["location"], uid=event_id)
        return event
