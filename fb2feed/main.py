# Copyright (c) 2019 - Philippe Normand
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

from xdg import XDG_CONFIG_HOME
import asyncio
import os, sys
import time
import logging
import optparse
import psutil
import toml

from . import feed_maker, __version__, utils

def house_keeping(media_dir):
    if not os.path.isdir(media_dir):
        return

    # Remove files older than 30 days.
    current_time = time.time()
    for f in os.listdir(media_dir):
        full_path = os.path.join(media_dir, f)
        creation_time = os.path.getctime(full_path)
        if (current_time - creation_time) // (24 * 3600) >= 30:
            os.unlink(full_path)

def kill_xdg_desktop_portal():
    for proc in psutil.process_iter():
        if proc.name().endswith('xdg-desktop-portal'):
            proc.kill()

def main(args=None):
    if not args:
        args = sys.argv[1:]

    parser = optparse.OptionParser(usage="python3 -m fb2feed [options]", version="Fb2Feed %s" % __version__)
    parser.add_option("-d", "--debug", dest="debug", default=False, action="store_true",
                      help="Logs debug messages to stdout")
    parser.add_option("-v", "--verbose", dest="verbose", default=False, action="store_true",
                      help="Logs verbose debug messages to stdout")
    parser.add_option("-p", "--persistent-fluyt", dest="persistent_fluyt", default=False, action="store_true",
                      help="Rely on the persistent DBus Fluyt service")
    (options, args) = parser.parse_args(args)

    extra_logging_kws = {}
    if options.debug:
        extra_logging_kws['level'] = logging.DEBUG
    if options.verbose:
        extra_logging_kws['level'] = utils.REALLY_VERBOSE
    logging.basicConfig(format='%(asctime)s [%(levelname)-8s] %(message)s', **extra_logging_kws)

    if args:
        config_path = args[0]
    else:
        config_path = os.path.join(XDG_CONFIG_HOME, 'fb2feed.toml')

    try:
        config = toml.load(config_path)
    except toml.decoder.TomlDecodeError as error:
        logging.error("Invalid TOML config file: %s", error)
        return 1
    except Exception as exc:
        logging.error(exc)
        return 2

    kill_xdg_desktop_portal()

    maker = feed_maker.FeedMaker(config, options)
    tasks = maker.collect_tasks()
    loop = asyncio.get_event_loop()
    for task in tasks:
        loop.run_until_complete(task)

    media_dir = config['general']['media_directory']
    house_keeping(media_dir)
    return 0
