# Copyright (c) 2019 - Philippe Normand
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

from aiohttp import ClientSession
from importlib import resources
import asyncio
import dbussy
import json
import logging
import os
import youtube_dl

from . import utils

MAX_CONCURRENT_REQUESTS = 100

class YoutubeDlLogger(object):
    def debug(self, msg):
        logging.log(utils.REALLY_VERBOSE, msg)

    def warning(self, msg):
        # logging.warning(msg)
        pass

    def error(self, msg):
        logging.error(msg)


def YoutubeDlProgressHook(d):
    pass

class Client:
    def __init__(self):
        self.sem = asyncio.Semaphore(MAX_CONCURRENT_REQUESTS)
        self.session = ClientSession()
        self._dbus_proxy = None

    def semaphore(self):
        return self.sem

    async def __aenter__(self):
        await self.session.__aenter__()
        return self

    async def __aexit__(self, type, value, traceback):
        await self.session.__aexit__(type, value, traceback)

    async def _fetch(self, url):
        async with self.session.get(url) as response:
            return await response.read()

    async def fetch(self, url, media_dir):
        async with self.sem:
            destination = utils.url_filename(url)
            full_path = os.path.join(media_dir, destination)
            os.makedirs(media_dir, exist_ok=True)
            if os.path.isfile(full_path):
                return

            data = await self._fetch(url)
            f = open(full_path, 'wb')
            f.write(data)
            f.close()

    async def video_fetch(self, url, media_dir):
        ydl_opts = {
            'outtmpl': os.path.join(media_dir, '%(id)s.%(ext)s'),
            'logger': YoutubeDlLogger(),
            'progress_hooks': [YoutubeDlProgressHook],
        }
        async with self.sem:
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                try:
                    ydl.download([url])
                except youtube_dl.utils.DownloadError as error:
                    print(error)

    async def fetch_with_fluyt(self, url, js_script, response_type, persistent_service):
        subprocess_options = dict(stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
        js_package = "fb2feed.data"
        logging.debug("Fetching %s" % url)
        if persistent_service:
            if not self._dbus_proxy:
                self._dbus_proxy = await dbussy.Connection.bus_get_async(dbussy.DBUS.BUS_SESSION, private=False)
            with resources.open_text(js_package, resource=js_script) as js_script_fd:
                message = dbussy.Message.new_method_call(destination = "net.baseart.Fluyt.Server",
                                                         path = "/net/baseart/Fluyt/Object",
                                                         iface = "net.baseart.Fluyt.Interface",
                                                         method = "LoadUriAndEvaluateScript")
                message.append_objects((dbussy.BasicType(dbussy.TYPE.STRING),), url)
                message.append_objects((dbussy.BasicType(dbussy.TYPE.UNIX_FD),), js_script_fd.fileno())
                message.append_objects((dbussy.BasicType(dbussy.TYPE.STRING),), response_type)
                reply = await self._dbus_proxy.send_await_reply(message, timeout=dbussy.DBUS.TIMEOUT_INFINITE)
                payload = reply.all_objects[0]
        else:
            with resources.path(js_package, resource=js_script) as js_script_path:
                cmd = "flatpak run --filesystem=home net.baseart.Fluyt %s %s %s" % (url, js_script_path.as_posix(), response_type)
                proc = await asyncio.create_subprocess_shell(cmd, **subprocess_options)
                stdout, stderr = await proc.communicate()
                payload = stdout.decode("utf-8")
        if response_type == "json":
            try:
                return json.loads(payload)
            except json.JSONDecodeError:
                logging.error(payload)
                return {}
        else:
            return payload
